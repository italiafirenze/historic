<?php get_header(); ?>
	
<section>

	<div class="banner--hero">
		<div class="overlay">
			<div class="row">
				<div class="small-12 columns">
					<h1><?php echo of_get_option('hero_headline', 'The headline goes here'); ?></h1>
					<h3 class="subheader"><?php echo of_get_option('hero_subheader', 'The subheadline goes here'); ?></h3>
					<a class="large button call-to-action" href="<?php echo of_get_option('hero_button_url', '/cars-for-sale/'); ?>"><?php echo of_get_option('hero_button_text', 'View our Prestige Cars'); ?></a>
					<br>
				</div>
			</div>
		</div>
	</div>
	
</section>

<section id="callout--text">
	
	<div class="row">
		<div class="medium-4 large-3 columns">
		 <h3><?php echo of_get_option('home_section1_headline', 'Performance, Classic and Race Cars'); ?></h3>
		 <h5 class="subheader"><?php echo of_get_option('home_section1_subheader', 'Hand chosen specialist cars in Shropshire'); ?></h5>
		</div>
		<div class="medium-8 large-9 columns columnar">	
			<p><?php echo of_get_option('home_section1_body', 'Add description here'); ?><br><a class="bright" href="<?php echo of_get_option('home_section1_link_url', '#'); ?>"><?php echo of_get_option('home_section1_link', 'Browse Cars'); ?></a></p>		
		</div>
		
	</div>
	
	<hr>
	
	<div class="row">
		<div class="medium-4 large-3 columns">
		 <h3><?php echo of_get_option('home_section2_headline', 'Historic Motorsport Memorablia'); ?></h3>
		 <h5 class="subheader"><?php echo of_get_option('home_section2_subheader', 'Including parts, rarities and collectibles'); ?></h5>
		</div>
		<div class="medium-8 large-9 columns columnar">
			<p><?php echo of_get_option('home_section2_body', 'Add description here'); ?><br><a class="bright" href="<?php echo of_get_option('home_section2_link_url', '#'); ?>"><?php echo of_get_option('home_section2_link', 'Shop Memorablia'); ?></a></p>			
		</div>
		
	</div>
	
	<hr>
	
</section>

<section id="featured-cars">
	
	<div class="row">
	
		<div class="medium-4 large-3 columns">
			 <h3>Featured Cars</h3>
			 <h5 class="subheader"><a class="bright" href="/cars-for-sale/">See all our cars</a></h5>
		</div>
		
		<div class="medium-8 large-9 columns">
			<?php require_once(get_template_directory().'/partials/loop-car_mini.php'); ?>		
		</div>
		
	</div>
			
</section>
				
<?php get_footer(); ?>