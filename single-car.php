<?php
/*
Template Name: Custom Page Example
*/
?>

<?php get_header(); ?>

	<article id="car-<?php the_ID(); ?>" <?php post_class(''); ?> role="product" itemscope itemtype="http://schema.org/WebPage">
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="s-page-title">
		
			<div class="row">
			
				<div class="small-12 columns">
					<h1 id="car-single__title" class="syst"><?php the_title(); ?><?php if((get_post_meta($post->ID, "wcs_price", true))) : ?> <small><?php echo get_post_meta($post->ID, 'wcs_price', true); ?></small> <?php endif; ?></h1>
				</div>
				
			</div>	
		
		</div> <!-- end article header -->
			
	
	<section class="s-car-single__images">
		
		<div class="row">
			<div class="medium-12 columns medium-centered">	
						
				<div class="master-slider ms-skin-default" id="masterslider">
				<?php
							$args = array(
							'post_type' => 'attachment',
							'numberposts' => -1,
							'post_status' => null,
							'post_mime_type' => 'image',
							'post_parent' => $post->ID,
							'orderby'  => 'menu_order',
							'order' => 'ASC',
						);
						$attachments = get_posts( $args );
						if ( $attachments ) {
						foreach ( $attachments as $attachment ) {
										$image_attributes = wp_get_attachment_image_src ($attachment->ID, 'car-full');
										echo '<div class="ms-slide">';
										echo '<img src="';
										echo $image_attributes[0] ;
										echo '"/>';	
										//echo '<a href="';
										//echo $image_attributes[0] ;
										//echo '" target="_blank"/>';
										//echo '</a>';
										echo '</div>';
						}
						}
						
						;?>
				</div>
				
			</div>
		</div>
		

		<script>
		    var slider = new MasterSlider();
	 
		    slider.control('arrows' ,{insertTo:'#masterslider'});  
		    slider.control('bullets'); 
		 
		    slider.setup('masterslider' , {
		        width:1200,
		        height:900,
		        space:5,
		        view:'basic',
		        //layout:'fullscreen',
		        fullscreenMargin:57,
		        speed:75
		    });	
	    </script>	
		
		</section>
		
	<?php  if((get_post_meta($post->ID, "wcs_headline", true))) { ?>
		<section class="s-car-single__headline page-section">
			<div class="row">
				<div class="small-12 columns">
					<h2 class="headline headline--light"><?php echo get_post_meta($post->ID, 'wcs_headline', true); ?></h2>
				</div>
			</div>
		</section>
	<?php } ?>
	
		<div id="content">
		
		<div id="inner-content" class="row">
		
		<div id="main" class="small-12 columns" role="main">
		
		<!--
		<div class="row">
		
			<?php  if((get_the_term_list($post->ID,  'plateyear', '', ', ', ''))) { ?>
			<div class="medium-3 columns car-single__info">
			<p class="car-single__type">Year</p>
			<p class="car-single__detail"><?php $terms_as_text = get_the_term_list($post->ID,  'plateyear', '', ', ', '');
if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></p>
			</div>
			<?php } ?>
			
			<?php  if((get_post_meta($post->ID, "wcs_mileage", true))) { ?>
			<div class="medium-3 columns car-single__info">
				<p class="car-single__type">Mileage</p>
				<p class="car-single__detail"><?php echo get_post_meta($post->ID, 'wcs_mileage', true); ?></p>
			</div>
			<?php } ?>
			
			<?php  if((get_post_meta($post->ID, "wcs_vrm", true))) { ?>
			<div class="medium-3 columns car-single__info">
				<p class="car-single__type">Year</p>
				<p class="car-single__detail"><?php echo get_post_meta($post->ID, 'wcs_vrm', true); ?></p>
			</div>
			<?php } ?>
		
		</div> -->
		
		
		<div class="row">
		
			<section class="s-car-single__details">
			<h3 class="headline bold">Details</h3>
			<div class="car-single__details">
			<dl>
			
				<?php  if((get_post_meta($post->ID, "wcs_year", true))) { ?>
			<dt>Year</dt>
			<dd><?php echo get_post_meta($post->ID, 'wcs_year', true) ?></dd>
				<?php } ?>			
			
				<?php  if((get_post_meta($post->ID, "wcs_vrm", true))) { ?>
			<dt>VRM</dt>
			<dd><?php echo get_post_meta($post->ID, 'wcs_vrm', true); ?></dd>
				<?php } ?>
			
				<?php  if((get_post_meta($post->ID, "wcs_mileage", true))) { ?>
			<dt>Mileage</dt>
			<dd><?php echo get_post_meta($post->ID, 'wcs_mileage', true); ?></dd>
				<?php } ?>
				
				<?php  if((get_the_term_list($post->ID,  'fuel', '', ', ', ''))) { ?>
			<dt>Fuel</dt>
			<dd><?php $terms_as_text = get_the_term_list($post->ID,  'fuel', '', ', ', '');
if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></dd>
				<?php } ?>
				
				<?php  if((get_the_term_list($post->ID,  'gearbox', '', ', ', ''))) { ?>
			<dt>Gearbox</dt>
			<dd><?php $terms_as_text = get_the_term_list($post->ID,  'gearbox', '', ', ', '');
if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></dd>
				<?php } ?>
				
				<?php  if((get_the_term_list($post->ID,  'taxclass', '', ', ', ''))) { ?>
			<dt>Tax Class</dt>
			<dd><?php $terms_as_text = get_the_term_list($post->ID,  'taxclass', '', ', ', '');
if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></dd>
				<?php } ?>
				
				<?php  if((get_post_meta($post->ID, "wcs_colour", true))) { ?>
			<dt>Colour</dt>
			<dd itemprop="color"><?php echo get_post_meta($post->ID, 'wcs_colour', true); ?></dd>
				<?php } ?>
				
				<?php  if((get_post_meta($post->ID, "wcs_interior", true))) { ?>
			<dt>Interior</dt>
			<dd><?php echo get_post_meta($post->ID, 'wcs_interior', true); ?></dd>
				<?php } ?>
			
				<?php  if((get_post_meta($post->ID, "wcs_enginesize", true))) { ?>
			<dt>Engine Size</dt>
			<dd><?php echo get_post_meta($post->ID, 'wcs_enginesize', true) ?></dd>
				<?php } ?>
			
				<?php  if((get_post_meta($post->ID, "wcs_power", true))) { ?>
			<dt>Power</dt>
			<dd><?php echo get_post_meta($post->ID, 'wcs_power', true); ?></dd>
				<?php } ?>
				
				<?php  if((get_post_meta($post->ID, "wcs_emmissions", true))) { ?>
			<dt>Emmissions</dt>
			<dd><?php echo get_post_meta($post->ID, 'wcs_emmissions', true); ?></dd>
				<?php } ?>
							
			
			</dl>
		</section>
		
	    <section class="entry-content s-car-single__description" itemprop="description">
	    	<h3 class="headline bold">Description</h3>
	    	<div class="car-single__description">
	    		<?php the_content(); ?>
	    		<ul class="car-single__added">
	    			<li>Part-exchange accepted
	    			<li>Viewing 7 days by appointment
	    			<li>Finance arranged
	    		</ul>
	    	</div>
		</section> <!-- end article section -->
				

		</div>
							
			    						
		</div> <!-- end #main -->
		
		</div> <!-- end #inner-content -->
		
		</div> <!-- end #content -->
			
			<footer class="car-single__footer">
			
				<div class="row">
					<div class="small-12 columns">
						<h2 class="headline call-to-action">
						Call <?php echo of_get_option('telephone', 'us'); ?> or <a href="/contact/">email now</a> about this car
						</h2>
					</div>
				</div>
			
			</footer> <!-- end article footer -->
			
			<?php endwhile; else : ?>

		<?php endif; ?>
						
	</article> <!-- end article -->		
			
		
<?php get_footer(); ?>
