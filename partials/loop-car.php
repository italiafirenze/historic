<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<section class="car-single__images">
		
		<div class="row">
			<div class="medium-12 columns medium-centered">	
						
				<div class="master-slider ms-skin-default" id="masterslider">
				<?php
							$args = array(
							'post_type' => 'attachment',
							'numberposts' => -1,
							'post_status' => null,
							'post_mime_type' => 'image',
							'post_parent' => $post->ID,
							'orderby'  => 'menu_order',
							'order' => 'ASC',
						);
						$attachments = get_posts( $args );
						if ( $attachments ) {
						foreach ( $attachments as $attachment ) {
										$image_attributes = wp_get_attachment_image_src ($attachment->ID, 'car-image');
										echo '<div class="ms-slide">';
										echo '<img src="';
										echo $image_attributes[0] ;
										echo '"/>';	
										echo '<a href="';
										echo $image_attributes[0] ;
										echo '" target="_blank"/>';
										echo '</a>';
										echo '</div>';
						}
						}
						
						;?>
				</div>
				
			</div>
		</div>
		

		<script>
		    var slider = new MasterSlider();
		    slider.setup('masterslider' , {
		            width:500,    // slider standard width
		            height:334,   // slider standard height
		            autoHeight:true,
					view:'partialWave',
					layout:'partialview',
		            loop: "true",
		            overPause: "true"
		            // more slider options goes here...
		        });
		    // adds Arrows navigation control to the slider.
		    slider.control('arrows');
		</script>	
		
		</section>
		
	<?php  if((get_post_meta($post->ID, "wcs_headline", true))) { ?>
		<section class="s-car-single__headline page-section">
			<div class="row">
				<div class="small-12 columns text-center">
					<h2 class="car-single__headline"><?php echo get_post_meta($post->ID, 'wcs_headline', true); ?></h2>
				</div>
			</div>
		</section>
	<?php } ?>
		
		<div class="row">
		
	    <section class="entry-content s-car-single__description" itemprop="description">
		    <?php the_content(); ?>
		</section> <!-- end article section -->
				
		<section class="s-car-single__details">
			<dl>
					<?php  if((get_post_meta($post->ID, "wcs_year", true))) { ?>
				<dt>Year</dt>
				<dd><?php echo get_post_meta($post->ID, 'wcs_year', true); ?></dd>
					<?php } ?>
				
					<?php  if((get_the_term_list($post->ID,  'fuel', '', ', ', ''))) { ?>
				<dt>Fuel</dt>
				<dd><?php $terms_as_text = get_the_term_list($post->ID,  'fuel', '', ', ', '');
	if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></dd>
					<?php } ?>
					
					<?php  if((get_the_term_list($post->ID,  'gearbox', '', ', ', ''))) { ?>
				<dt>Gearbox</dt>
				<dd><?php $terms_as_text = get_the_term_list($post->ID,  'gearbox', '', ', ', '');
	if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></dd>
					<?php } ?>
				
					<?php  if((get_post_meta($post->ID, "wcs_vrm", true))) { ?>
				<dt>VRM</dt>
				<dd><?php echo get_post_meta($post->ID, 'wcs_vrm', true); ?></dd>
					<?php } ?>
				
					<?php  if((get_post_meta($post->ID, "wcs_mileage", true))) { ?>
				<dt>Mileage</dt>
				<dd><?php echo get_post_meta($post->ID, 'wcs_mileage', true); ?></dd>
					<?php } ?>
					
					<?php  if((get_post_meta($post->ID, "wcs_colour", true))) { ?>
				<dt>Colour</dt>
				<dd itemprop="color"><?php echo get_post_meta($post->ID, 'wcs_colour', true); ?></dd>
					<?php } ?>
					
					<?php  if((get_post_meta($post->ID, "wcs_interior", true))) { ?>
				<dt>Interior</dt>
				<dd><?php echo get_post_meta($post->ID, 'wcs_interior', true); ?></dd>
					<?php } ?>
				
					<?php  if((get_post_meta($post->ID, "wcs_enginesize", true))) { ?>
				<dt>Engine Size</dt>
				<dd><?php echo get_post_meta($post->ID, 'wcs_enginesize', true) ?></dd>
					<?php } ?>
				
					<?php  if((get_post_meta($post->ID, "wcs_power", true))) { ?>
				<dt>Power</dt>
				<dd><?php echo get_post_meta($post->ID, 'wcs_power', true); ?> bhp</dd>
					<?php } ?>
					
					<?php  if((get_post_meta($post->ID, "wcs_emmissions", true))) { ?>
				<dt>Emmissions</dt>
				<dd><?php echo get_post_meta($post->ID, 'wcs_emmissions', true); ?> g/km</dd>
					<?php } ?>
								
			</dl>
		</section>
		
		</div>
							
		<footer class="article-footer">
			
		</footer> <!-- end article footer -->
	
<?php endwhile; else : ?>
					
	<?php get_template_part( 'partials/content', 'missing' ); ?>

<?php endif; ?>
							