<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="car-<?php the_ID(); ?>" <?php post_class('loop-car__item'); ?> role="product">
	<div class="row">
		<div class="medium-4 columns">
			<?php if ( has_post_thumbnail()) : ?>
				<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('car-half'); ?></a>
			<?php else : ?>
				<a href="<?php the_permalink() ?>"><img src="http://placehold.it/450x300&text=Car Picture"></a>
			<?php endif; ?>
				
				
		</div>
		
		<div class="medium-8 columns">
		
		<div class="row">
			<div class="medium-12 columns">
				<header class="article-header">
				<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
				</header> <!-- end article header -->
			</div>
			<div class="medium-9 columns">
				
				<section class="entry-content" itemprop="description">
			
					<?php // the_excerpt('<button class="tiny secondary">Read more...</button>'); ?>
					
					<div class="row">
						<div class="large-12 columns">
						
							 <div class="car-single__details">
								<dl>
								
									<?php  if((get_post_meta($post->ID, "wcs_mileage", true))) { ?>
								<dt>Mileage</dt>
								<dd><?php echo get_post_meta($post->ID, 'wcs_mileage', true); ?></dd>
									<?php } ?>
								
									<?php  if((get_the_term_list($post->ID,  'fuel', '', ', ', ''))) { ?>
								<dt>Fuel</dt>
								<dd><?php $terms_as_text = get_the_term_list($post->ID,  'fuel', '', ', ', '');
					if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></dd>
									<?php } ?>
									
									<?php  if((get_the_term_list($post->ID,  'gearbox', '', ', ', ''))) { ?>
								<dt>Gearbox</dt>
								<dd><?php $terms_as_text = get_the_term_list($post->ID,  'gearbox', '', ', ', '');
					if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></dd>
									<?php } ?>
								
									<?php  if((get_post_meta($post->ID, "wcs_vrm", true))) { ?>
								<dt>VRM</dt>
								<dd><?php echo get_post_meta($post->ID, 'wcs_vrm', true); ?></dd>
									<?php } ?>

								</dl>
								</div>

					
						<!--<ul class="medium-block-grid-2">
								<li>
									<ul>
										<?php  if((get_the_term_list($post->ID,  'plateyear', '', ', ', ''))) { ?>
							<li><span>Year: </span><?php $terms_as_text = get_the_term_list($post->ID,  'plateyear', '', ', ', '');
				if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></li>
								<?php } ?>
								
								<?php  if((get_the_term_list($post->ID,  'fuel', '', ', ', ''))) { ?>
							<li><span>Fuel: </span><?php $terms_as_text = get_the_term_list($post->ID,  'fuel', '', ', ', '');
				if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></li>
								<?php } ?>
								
								<?php  if((get_the_term_list($post->ID,  'gearbox', '', ', ', ''))) { ?>
							<li><span>Gearbox: </span><?php $terms_as_text = get_the_term_list($post->ID,  'gearbox', '', ', ', '');
				if (!empty($terms_as_text)) echo '', strip_tags($terms_as_text) ,''; ?></li>
								<?php } ?>
										</ul>
									</li>
									<li>
										<ul>
											<?php  if((get_post_meta($post->ID, "wcs_mileage", true))) { ?>
										<li><span>Mileage: </span><?php echo get_post_meta($post->ID, 'wcs_mileage', true); ?></li>
											<?php } ?>
											
											<?php  if((get_post_meta($post->ID, "wcs_colour", true))) { ?>
										<li itemprop="color"><span>Colour: </span><?php echo get_post_meta($post->ID, 'wcs_colour', true); ?></li>
											<?php } ?>
											
											<?php  if((get_post_meta($post->ID, "wcs_interior", true))) { ?>
										<li><span>Interior: </span><?php echo get_post_meta($post->ID, 'wcs_interior', true); ?></li>
											<?php } ?>
				
										</ul>
									</li>
								</ul>-->
																			
								<p><a href="#">Contact about this car</a> | <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">View more details</a></p>
								
						</div>
					</div>
	
				</section> <!-- end article section -->
				
			</div>
			
			<div class="medium-3 columns">
				<?php if((get_post_meta($post->ID, "wcs_price", true))) : ?>
			<h4 id="car-price" class="price" itemprop="price"><strong><?php echo get_post_meta($post->ID, 'wcs_price', true); ?></strong></h4>
				<?php endif; ?>
				<a href="<?php the_permalink() ?>" class="small call-to-action button secondary expand medium">Details</a>
			</div>
			
	</div>
</div>

</article> <!-- end article -->

<hr>

<?php endwhile; ?>	
					
<?php joints_page_navi(); ?>

<?php else : ?>
	<?php get_template_part( 'partials/content', 'missing' ); ?>
<?php endif; ?>
