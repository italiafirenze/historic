<?php
	$temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query();
	$wp_query->query('showposts=3&post_type=car&orderby=rand'.'&paged='.$paged);
?>
<div class="loop-car row">
	<?php  while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
		<div class="medium-4 large-4 columns">
			<div class="car-loop__item">
				<a href="<?php the_permalink();?>">
						<div class="car-loop__title">
						<h4 class="headline"><?php the_title()?><br><small><?php echo get_post_meta($post->ID, 'wcs_price', true); ?></small></h4>
						</div>
						
						<div class="car-loop__image">
						<?php if ( has_post_thumbnail()):
							
							the_post_thumbnail( 'car-half', array( 'class' => 'car-image') );
							
							else: echo "<img src='http://placehold.it/600x450?text=Photo+Coming+Soon'>";
							
						endif; ?>
						</div>
						
				</a>
			</div>
		</div>
	<?php endwhile; ?>
</div>

<?php
	$wp_query = null;
	$wp_query = $temp;  // Reset
?>