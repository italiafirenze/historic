<?php get_header(); ?>

		<div class="s-page-title">
		
			<div class="row">
			
				<div class="small-12 columns">
					<h1 id="page__title"><?php the_title()?></h1>
				</div>
				
			</div>	
		
		</div> <!-- end article header -->
			
			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="small-12 columns" role="main">
						
						<!-- To see additional archive styles, visit the /partials directory -->
						<div class="search-results">
					    	<?php get_template_part( 'partials/loop', 'archive-car' ); ?>
						</div>		
								
				    </div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>