<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet (lowercase and without spaces)
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);

	// echo $themename;
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'options_check'),
		'two' => __('Two', 'options_check'),
		'three' => __('Three', 'options_check'),
		'four' => __('Four', 'options_check'),
		'five' => __('Five', 'options_check')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'options_check'),
		'two' => __('Pancake', 'options_check'),
		'three' => __('Omelette', 'options_check'),
		'four' => __('Crepe', 'options_check'),
		'five' => __('Waffle', 'options_check')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();

	$options[] = array(
		'name' => __('General Settings', 'options_check'),
		'type' => 'heading');
		
	$options[] = array(
		'name' => __('Here is where you can update things that are displayed in the front end of the website.', 'options_check'),
		'type' => 'info');
		
	$options[] = array(
		'name' => __('Postal Address', 'options_check'),
		'desc' => __('In one long line, separated by commas.', 'options_check'),
		'id' => 'address',
		'std' => 'Address',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Telephone Number', 'options_check'),
		'desc' => __('The telephone number to contact the company on.', 'options_check'),
		'id' => 'telephone',
		'std' => 'Telephone Number',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Email Address', 'options_check'),
		'desc' => __('The email address to contact the company on.', 'options_check'),
		'id' => 'emailaddress',
		'std' => 'Email Address',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Company Reg Number', 'options_check'),
		'id' => 'company-number',
		'std' => 'Company Number',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('VAT Number', 'options_check'),
		'id' => 'vat-number',
		'std' => 'VAT Number',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Home Page Settings', 'options_check'),
		'type' => 'heading');
	
	$options[] = array(
		'name' => __('Hero Headline', 'options_check'),
		'desc' => __('The main headline on the home page', 'options_check'),
		'id' => 'hero_headline',
		'std' => 'Headline goes here',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Hero Subheadline', 'options_check'),
		'desc' => __('The hero subheadline from the home-page', 'options_check'),
		'id' => 'hero_subheader',
		'std' => 'Subheader goes here',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Hero Button Text', 'options_check'),
		'desc' => __('The hero button text from the home-page', 'options_check'),
		'id' => 'hero_button_text',
		'std' => 'Button text goes here',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Hero Button URL', 'options_check'),
		//'desc' => __('The hero button URL from the home-page', 'options_check'),
		'id' => 'hero_button_url',
		'std' => 'URL goes here',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Home Page First Section Header', 'options_check'),
		'desc' => __('The section headline from the home-page', 'options_check'),
		'id' => 'home_section1_headline',
		'std' => 'Headline goes here',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Home Page First Section Subheader', 'options_check'),
		'desc' => __('The hero subheadline from the home-page', 'options_check'),
		'id' => 'home_section1_subheader',
		'std' => 'Subheadline goes here',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Home Page First sText Area', 'options_check'),
		'desc' => __('Home page first section text area', 'options_check'),
		'id' => 'home_section1_body',
		'std' => 'Add your text',
		'type' => 'textarea');
		
	$options[] = array(
		'name' => __('Home Page First Section Link Text', 'options_check'),
		'desc' => __('The section link', 'options_check'),
		'id' => 'home_section1_link',
		'std' => 'Link text goes here',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Home Page First Section Link URL', 'options_check'),
		'desc' => __('Enter the url including the http://', 'options_check'),
		'id' => 'home_section1_link_url',
		'std' => 'http://#',
		'type' => 'text');
	
		$options[] = array(
		'name' => __('Home Page First Section Header', 'options_check'),
		'desc' => __('The section headline from the home-page', 'options_check'),
		'id' => 'home_section2_headline',
		'std' => 'Headline goes here',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Home Page First Section Subheader', 'options_check'),
		'desc' => __('The hero subheadline from the home-page', 'options_check'),
		'id' => 'home_section2_subheader',
		'std' => 'Subheadline goes here',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Home Page Second Text Area', 'options_check'),
		'desc' => __('Home page second section text area', 'options_check'),
		'id' => 'home_section2_body',
		'std' => 'Add your text',
		'type' => 'textarea');
		
	$options[] = array(
		'name' => __('Home Page First Section Link Text', 'options_check'),
		'desc' => __('The section link', 'options_check'),
		'id' => 'home_section2_link',
		'std' => 'Link text goes here',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Home Page First Section Link URL', 'options_check'),
		'desc' => __('Enter the url including the http://', 'options_check'),
		'id' => 'home_section2_link_url',
		'std' => 'http://#',
		'type' => 'text');
			
	/**
	 * For $settings options see:
	 * http://codex.wordpress.org/Function_Reference/wp_editor
	 *
	 * 'media_buttons' are not supported as there is no post to attach items to
	 * 'textarea_name' is set by the 'id' you choose
	 */

	
	return $options;
}