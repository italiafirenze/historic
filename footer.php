					<footer class="footer" role="contentinfo">
						<div class="row">
						<hr>
							<div class="medium-6 columns">
								<img class="garage-image" src="/wp-content/themes/historic-motorsport/library/images/garage/garage4.jpg" alt="Sports and race cars in Shrewsbury from Historic Motorsport">
							</div>
							
							<div class="medium-6 columns">
								<img class="garage-image" src="/wp-content/themes/historic-motorsport/library/images/garage/garage7.jpg" alt="Sports and race cars in Shrewsbury from Historic Motorsport">
							</div>
						</div>
						<div id="inner-footer" class="row">
						<hr>
							
							<div class="large-10 medium-9 columns">
							
								<nav role="navigation">
									<?php joints_footer_links(); ?>
								</nav>
								
								<p class="company-details">
								<?php echo of_get_option('address', ''); ?> | Call <?php echo of_get_option('telephone', ''); ?> | Company # <?php echo of_get_option('company-number', ''); ?>
								</p>
								
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> Historic Motorsport Ltd. Made by<a href="https://pilgrim.marketing" target="_blank">Pilgrim Marketing</a>.</p>
								
							</div>
							
							<div class="large-2 medium-3 columns">
							
								<div class="logo">
									<a href="#inner-header" title="Return to top">
										<img src="/wp-content/themes/historic-motorsport/library/images/historic-logo-trans-350.png">
									</a>
								</div>
								
							</div>
							
						</div> <!-- end #inner-footer -->
						
					</footer> <!-- end .footer -->
				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->
		<!-- all js scripts are loaded in library/joints.php -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->